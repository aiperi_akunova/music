const express = require('express');
const User = require("../models/User");
const TrackHistory = require("../models/TrackHistory");
const router = express.Router();

router.post('/', async (req, res) => {

  const token = req.get('Authorization');

  if(!token){
    return res.status(401).send({error: "No token present"});
  }

  const user = await User.findOne({token});

  if(!user){
    return res.status(401).send({error: "Wrong token"})
  }

  const date = new Date();
  const trackHistoryData = {
    user: user.id,
    track: req.body.track,
    datetime: date,
  };

  const trackHistory = new TrackHistory(trackHistoryData);

  try {
    await trackHistory.save();
    res.send(trackHistory);
  } catch (e) {
    res.status(400).send(e);
  }

});

module.exports = router;